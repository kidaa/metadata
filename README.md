# metadata
metadata development - formats and machinery for GITenberg

![travis status](https://img.shields.io/travis/gitenberg-dev/metadata.svg)
![PyPI version](https://img.shields.io/pypi/v/gitenberg.metadata.svg)

## Installation

These metadata tools are hosted on PyPI as the namespaced package [gitenberg.metadata](https://pypi.python.org/pypi/gitenberg.metadata/)

    pip install gitenberg.metadata
